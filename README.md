# OpenML dataset: robot-failures-lp1

https://www.openml.org/d/1516

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Luis Seabra Lope, Luis M. Camarinha-Matos    
**Source**: UCI   
**Please cite**:   

* Dataset Title:   
Robot Execution Failures Data Set 

* Abstract:   
This dataset contains force and torque measurements on a robot after failure detection. Each failure is characterized by 15 force/torque samples collected at regular time intervals


* Source:  

Original Owner and Donor

Luis Seabra Lopes and Luis M. Camarinha-Matos, Universidade Nova de Lisboa, Monte da Caparica, Portugal 


* Data Set Information:

The donation includes 5 datasets, each of them defining a different learning problem: 

* LP1 (This Dataset): failures in approach to grasp position   
* LP2: failures in transfer of a part   
* LP3: position of part after a transfer failure   
* LP4: failures in approach to ungrasp position   
* LP5: failures in motion with part   

In order to improve classification accuracy, a set of five feature transformation strategies (based on statistical summary features, discrete Fourier transform, etc.) was defined and evaluated. This enabled an average improvement of 20% in accuracy. The most accessible reference is [Seabra Lopes and Camarinha-Matos, 1998].


* Attribute Information:

All features are numeric although they are integer valued only. Each feature represents a force or a torque measured after failure detection; each failure instance is characterized in terms of 15 force/torque samples collected at regular time intervals starting immediately after failure detection; The total observation window for each failure instance was of 315 ms. 

Each example is described as follows: 

class    
Fx1 Fy1 Fz1 Tx1 Ty1 Tz1    
Fx2 Fy2 Fz2 Tx2 Ty2 Tz2    
......    
Fx15 Fy15 Fz15 Tx15 Ty15 Tz15   

where Fx1 ... Fx15 is the evolution of force Fx in the observation window, the same for Fy, Fz and the torques; there is a total of 90 features.


* Relevant Papers:

Seabra Lopes, L. (1997) "Robot Learning at the Task Level: a Study in the Assembly Domain", Ph.D. thesis, Universidade Nova de Lisboa, Portugal. 

Seabra Lopes, L. and L.M. Camarinha-Matos (1998) Feature Transformation Strategies for a Robot Learning Problem, "Feature Extraction, Construction and Selection. A Data Mining Perspective", H. Liu and H. Motoda (edrs.), Kluwer Academic Publishers. 

Camarinha-Matos, L.M., L. Seabra Lopes, and J. Barata (1996) Integration and Learning in Supervision of Flexible Assembly Systems, "IEEE Transactions on Robotics and Automation", 12 (2), 202-219.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1516) of an [OpenML dataset](https://www.openml.org/d/1516). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1516/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1516/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1516/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

